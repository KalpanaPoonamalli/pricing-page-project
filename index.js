const checkbox = document.getElementById('checkbox');
const priceNumbers = document.getElementsByClassName('price-number');

checkbox.addEventListener('change', () => {
    const isChecked = checkbox.checked;
    // console.log(isChecked);
    // console.log(priceNumbers)
    if (isChecked){
        // console.log(priceNumbers[0].innerHTML)
        priceNumbers[0].innerHTML = 19.99;
        priceNumbers[1].innerHTML = 24.99;
        priceNumbers[2].innerHTML = 39.99;
    } else {
        priceNumbers[0].innerHTML = 199.99;
        priceNumbers[1].innerHTML = 249.99;
        priceNumbers[2].innerHTML = 399.99;
    }
}) 